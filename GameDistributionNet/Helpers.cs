﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace Vimeo
{
    public static class Helpers
    {
        public static IWebProxy Proxy = null;

        public const string GOOGLE_CLOUD_ASSETS_BUCKET = "gd-assets";
        public const string GOOGLE_CLOUD_GAMES_BUCKET = "gd-games";
        public const string GOOGLE_CLOUD_GAMES_FLASH_BUCKET = "gd-games-flash";

        public static byte[] ToByteArray(string s)
        {
            return Encoding.UTF8.GetBytes(s);
        }

        public static string ToBase64(string s)
        {
            return Convert.ToBase64String(ToByteArray(s));
        }

        public static string ComputeHash(HashAlgorithm hashAlgorithm, string data)
        {
            if (hashAlgorithm == null)
                throw new ArgumentNullException("hashAlgorithm");

            if (string.IsNullOrEmpty(data))
                throw new ArgumentNullException("data");

            byte[] dataBuffer = Encoding.ASCII.GetBytes(data);
            byte[] hashBytes = hashAlgorithm.ComputeHash(dataBuffer);
            return Convert.ToBase64String(hashBytes);
        }

        public static string PercentEncode(string value)
        {
            const string unreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
            var result = new StringBuilder();

            foreach (char symbol in value)
            {
                if (unreservedChars.IndexOf(symbol) != -1)
                    result.Append(symbol);
                else
                    result.Append('%' + String.Format("{0:X2}", (int)symbol));
            }

            return result.ToString();
        }

        public static string KeyValueToString(Dictionary<string, string> payload)
        {
            string body = "";
            foreach (var item in payload)
                body += String.Format("{0}={1}&",
                    Helpers.PercentEncode(item.Key),
                    Helpers.PercentEncode(item.Value));
            if (body[body.Length - 1] == '&') body = body.Substring(0, body.Length - 1);
            return body;
        }

        public static string HTTPFetch(string url, string method, 
            WebHeaderCollection headers, Dictionary<string, string> payload,
            string contentType = "application/x-www-form-urlencoded")
        {
            return HTTPFetch(url, method, headers, KeyValueToString(payload), contentType);
        }

        public static string HTTPFetch(string url, string method, 
            WebHeaderCollection headers, string payload,
            string contentType = "application/x-www-form-urlencoded")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            if (Proxy != null) request.Proxy = Proxy;

            request.Headers = headers;
            request.Method = method;
            request.Accept = "application/vnd.vimeo.*+json; version=3.2";
            request.ContentType = contentType;
            request.KeepAlive = false;

            if (!String.IsNullOrWhiteSpace(payload))
            {
                var streamBytes = Helpers.ToByteArray(payload);
                request.ContentLength = streamBytes.Length;
                Stream reqStream = request.GetRequestStream();
                reqStream.Write(streamBytes, 0, streamBytes.Length);
                reqStream.Close();
            }

            HttpWebResponse response = (HttpWebResponse)(request.GetResponse());
            Debug.WriteLine(((HttpWebResponse)response).StatusDescription);

            var dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();

            response.Close();

            Debug.WriteLine(String.Format("Response from URL {0}:", url), "HTTPFetch");
            Debug.WriteLine(responseFromServer, "HTTPFetch");
            return responseFromServer;
        }
    }
}
