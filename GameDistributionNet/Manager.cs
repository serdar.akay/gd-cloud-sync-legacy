﻿using IPCNet;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace GDSyncApi
{
    class Manager
    {
        private IPCService _ipcService;

        private Thread _gdDataActivity;
        private bool _gdDataActivityStop;

        public Manager(IPCService ipcService)
        {
            RegisterServiceHandlers(ipcService);

        }
        internal IPCService IPCService{
            get
            {
                return _ipcService;
            }
        }
        private void RegisterServiceHandlers(IPCService ipcService)
        {
            _ipcService = ipcService;

            ipcService.RegisterServiceStart(new InvokeHandler(ServiceStart));

            ipcService.RegisterServiceStop(new InvokeHandler(ServiceStop));

            ipcService.RegisterInvoke(new InvokeInfo { Method = "gd.nextfile", Handler = new InvokeHandler(GDFileNext), Description = "GD next file to upload" });
#if DEBUG
            //GDFileNext(null);
            ServiceStart(null);
#else
            ServiceStart(null);
#endif
        }
        JToken ServiceStart(JToken args)
        {
            if (_gdDataActivity == null)
            {
                _gdDataActivityStop = false;
                _gdDataActivity = new Thread(new ThreadStart(transferGdDataActivity));
                _gdDataActivity.Start();
            }

            return true;
        }
        JToken ServiceStop(JToken args)
        {
            if (_gdDataActivity != null)
            {
                _gdDataActivityStop = true;
                _gdDataActivity.Join();
                _gdDataActivity = null;
            }

            return true;
        }
        JToken GDFileNext(JToken args)
        {
            using (GDFileTransfer task = new GDFileTransfer(this, args))
            {
                return task.Run();
            }
        }
        public void transferGdDataActivity()
        {
            while (!_gdDataActivityStop)
            {
                Thread.Sleep(new TimeSpan(0, 0, 5));

                try
                {
                    GDFileNext(null);
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }
            }
        }

    }
}