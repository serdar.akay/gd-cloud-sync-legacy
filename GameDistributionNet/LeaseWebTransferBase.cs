﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace GDSyncApi
{

    class LeaseWebTransferBase:IDisposable
    {
        protected GDSyncApi.DbDataContext _db;
        protected GDSyncApi.Ny1DbDataContext _ny1db;
        protected Manager _manager;
        protected JToken _args;
        protected string _workingPath;

        public LeaseWebTransferBase(Manager manager, JToken args)
        {
            _args = args;
            _db = new DbDataContext();
            _ny1db = new Ny1DbDataContext();
            _workingPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            _manager = manager;
        }

        virtual public JToken Run()
        {
            return null;
        }

        public void Dispose()
        {

        }

        void disposeDbContext()
        {
            try
            {
                if (_db != null)
                {
                    _db.Dispose();
                    _db = null;
                }
                if (_ny1db != null)
                {
                    _ny1db.Dispose();
                    _ny1db = null;
                }
            }
            catch (Exception exc)
            {

            }
        }

        protected void Log(string format, params object[] args)
        {
            Console.WriteLine(String.Format("[{0}] File Transfer:{1}", System.DateTime.UtcNow, String.Format(format, args)));
        }
        protected void LogError(string format, params object[] args)
        {
            Console.Error.WriteLine(String.Format("[{0}] File Transfer:{1}", System.DateTime.UtcNow, String.Format(format, args)));
        }
    }
}
