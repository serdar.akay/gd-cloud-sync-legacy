﻿using Ionic.Zip;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Collections.Specialized;
using System.Net.FtpClient;
using System.Text;
using static System.Net.WebRequestMethods;
using Vimeo;
using Google.Cloud.Storage.V1;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Upload;

namespace GDSyncApi
{
    class GDFileTransfer : LeaseWebTransferBase
    {
        Game _file;
        Uri _targetLink;

        public GDFileTransfer(Manager manager, JToken args) : base(manager, args)
        {
        }

        public override JToken Run()
        {
            _file = _ny1db.APP_SyncGetGameFile().FirstOrDefault();
            //_file = _ny1db.APP_SyncGetGameFileTemp().FirstOrDefault();

            if (_file == null) return null;

            try
            {
                //Log("Transfering {0} to lease web", _file.Filename);
                UploadFile();

                _file.Link = _targetLink.ToString();
                _file.SyncForLeaseweb = 2;
            }
            catch (Exception exc)
            {
                LogError(exc.Message);
                _file.SyncForLeaseweb = 3;
                _ny1db.APP_SyncErrorLog(1, exc.Message, _file.GameMd5);
            }
            finally
            {
                _ny1db.SubmitChanges();
            }


            return null;
        }

        public void UploadFile()
        {
            string gameType = "";
            string filename = "";
            List<string> purgeList_ = new List<string>();

            //Ftp Login
            try
            {
                using (FtpClient ftpClient = new FtpClient())
                {
                    string leasewebUrl = "http://{0}.gamedistribution.com/{1}";
                    filename = string.Format("{0}", _file.Filename);

                    ftpClient.Host = "eu.hcs.hwcdn.net";
                    ftpClient.Credentials = new NetworkCredential("hw-hcs-n7k2q4p6-8252:vooxe-ftp-fee4e", "Chai4@oojohcooJ4!");
                    ftpClient.Connect();
                    ftpClient.SocketKeepAlive = false;
                    ftpClient.Port = 21;
                    ftpClient.DataConnectionType = FtpDataConnectionType.AutoPassive;
                    ftpClient.MaximumDereferenceCount = 1000;

                    Log("Connected to FTP.");

                    switch (_file.GameType)
                    {
                        case 1:
                            gameType = "swf";
                            break;
                        case 2:
                            gameType = "unity";
                            break;
                        case 3:
                            gameType = "apk";
                            break;
                        case 4:
                            gameType = "ipa";
                            break;
                        case 5:
                            gameType = "html5";
                            break;

                        default: throw new Exception();
                    }

                    _targetLink = new Uri(string.Format(leasewebUrl, gameType, filename));
                    if (gameType == "html5")
                    {
                        purgeList_.Add("http://" + gameType + "games.vooxe.com/" + filename);//adding to purgeList_ .html5 file
                        purgeList_.Add("http://" + gameType + ".gamedistribution.com/" + filename);//adding to purgeList_ .html5 file
                    }
                    else if (gameType == "swf")
                    {
                        purgeList_.Add("http://" + gameType + ".gamedistribution.com/?game=" + _file.GameMd5);//adding to purgeList_ .swf file
                    }
                    string downloadUrl = string.Format("http://www.gamedistribution.com/store_{0}/{1}", gameType, _file.Filename);
                    Log("Downloading " + downloadUrl);


                    using (var fileStream = (new WebClient()).OpenRead(new Uri(downloadUrl)))
                    {
                        using (MemoryStream memoryFileStream = new MemoryStream())
                        {
                            string uploadFile = string.Format("origin_storage/content/{0}/{1}", gameType, filename);
                            Log("Uploading file " + uploadFile);

                            //
                            // Copy read stream to zip stream
                            //
                            fileStream.CopyTo(memoryFileStream);

                            // Upload file to server
                            //UploadFileToFtp(ftpClient, uploadFile, memoryFileStream);
                            UploadFileToFtp(uploadFile, memoryFileStream);

                            // For html5 Games unzip file and place it
                            if (_file.GameType == 5)
                            {

                                Log("Extracting " + uploadFile);

                                Dictionary<string, MemoryStream> html5Files = UnZipToMemory(memoryFileStream);

                                Log("Extracted " + html5Files.Count + " file(s)");

                                int i = 0;

                                foreach (KeyValuePair<string, MemoryStream> html5File in html5Files)
                                {
                                    string filePath = Path.GetDirectoryName(string.Format("origin_storage/content/{0}/{1}/{2}", gameType, _file.GameMd5, html5File.Key));
                                    if (!ftpClient.DirectoryExists(filePath))
                                    {
                                        Log("Creating directory " + filePath);
                                        ftpClient.CreateDirectory(filePath);
                                    }

                                    string extractedFile = string.Format("origin_storage/content/{0}/{1}/{2}", gameType, _file.GameMd5, html5File.Key);

                                    if (Path.GetExtension(extractedFile) != "")
                                    {
                                        Log("Uploading file " + extractedFile);
                                        UploadFileToFtp(extractedFile, html5File.Value);
                                        // UploadFileToFtp(ftpClient, extractedFile, html5File.Value);
                                    }

                                    html5File.Value.Close();
                                    html5File.Value.Dispose();

                                    purgeList_.Add("http://" + gameType + "games.vooxe.com/" + _file.GameMd5 + "/" + html5File.Key);
                                    purgeList_.Add("http://" + gameType + ".gamedistribution.com/" + _file.GameMd5 + "/" + html5File.Key);
                                    Log("Uploaded file " + i);
                                    i = i + 2;
                                    if (i % 50 == 0)
                                    {
                                        purgeFiles(purgeList_);
                                        purgeList_.Clear();
                                    }
                                }

                                html5Files = null;

                            } // if html5
                              // Upload to Google
                            UploadFileToGoogle(filename, memoryFileStream);
                        }
                    }
                    purgeFiles(purgeList_);
                }
            }
            catch (Exception ex)
            {
                Log("Optimi CDN connection problem.");
                string downloadUrl = string.Format("http://www.gamedistribution.com/store_{0}/{1}", gameType, _file.Filename);
                using (var fileStream = (new WebClient()).OpenRead(new Uri(downloadUrl)))
                {
                    using (MemoryStream memoryFileStream = new MemoryStream())
                    {
                        fileStream.CopyTo(memoryFileStream);
                        UploadFileToGoogle(filename, memoryFileStream);
                    }
                }
                Log("File uploaded to Google successfully..");
            }
        }

        public void UploadFileToFtp(FtpClient ftpClient, string fileName, Stream fileData)
        {
            using (var ftpStream = ftpClient.OpenWrite(fileName, FtpDataType.Binary))
            {
                var buffer = new byte[8 * 1024];
                int count;
                while ((count = fileData.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ftpStream.Write(buffer, 0, count);
                }

                ftpStream.Close();
            }
        }

        public void UploadFileToFtp(string fileName, Stream fileData)
        {
            var request = (FtpWebRequest)WebRequest.Create("ftp://eu.hcs.hwcdn.net/" + fileName);

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential("hw-hcs-n7k2q4p6-8252:vooxe-ftp-fee4e", "Chai4@oojohcooJ4!");
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            using (var requestStream = request.GetRequestStream())
            {
                fileData.Seek(0, SeekOrigin.Begin);
                fileData.CopyTo(requestStream);
                requestStream.Close();
            }

            var response = (FtpWebResponse)request.GetResponse();
            Log("Upload done: {0}", response.StatusDescription);
            response.Close();
        }

        private Dictionary<string, MemoryStream> UnZipToMemory(Stream LocalZip)
        {
            var result = new Dictionary<string, MemoryStream>();
            LocalZip.Seek(0, SeekOrigin.Begin);
            using (ZipFile zip = ZipFile.Read(LocalZip))
            {
                foreach (ZipEntry e in zip)
                {
                    MemoryStream data = new MemoryStream();
                    e.Extract(data);
                    if (notAllowFiles(e.FileName) || (e.Attributes == FileAttributes.Directory)) continue;
                    data.Seek(0, SeekOrigin.Begin);
                    result.Add(e.FileName, data);
                }
            }
            return result;
        }

        private bool notAllowFiles(string filename)
        {
            switch (filename)
            {
                case ".exe":
                case ".dll":
                case ".aspx":
                case ".php":
                case ".cs":
                case ".asp":
                case ".cgi":
                case ".pl":
                    return true;
            }
            return false;
        }

        private void purgeFiles(List<string> url_)
        {

            string tanentId_ = "f00a8975ca034d1aa78182d3aa9f6169";
            string tokenId_ = "d8383d667e126aaa1fe5dc549600add7";
            string content_ = "";

            for (int i = 0; i < url_.Count; i++)
            {
                content_ += "\"" + url_[i] + "\",";
            }
            content_ = content_.Substring(0, content_.Length - 1);
            string URL = String.Format("https://cdnapi.optimicdn.com/api/v1/{0}/purge", tanentId_);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("X-Auth-Token", tokenId_);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //string json = "[\"" + content_ + "\"]";
                string json = "[" + content_ + "]";

                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
            LogError("Purge Completed...");
        }

        private bool UploadFileToGoogle(string fileName, Stream file)
        {
            if (file.Length <= 0)
            {
                return false;
            }
            else
            {
                try
                {
                    Log("Google process started..");

                    GoogleCredential credential;

                    using (var fs = new FileStream("Vooxe Gamedistribution-d2ccbbfc0e05.json", FileMode.Open, FileAccess.Read))
                    {
                        credential = GoogleCredential.FromStream(fs);
                    }

                    Log("Google credential took successfully..");

                    //var storage = StorageClient.Create(credential);

                    if (_file.GameType == 5)
                    {
                        PushFileToGoogle(credential, Helpers.GOOGLE_CLOUD_GAMES_BUCKET, fileName, "application/octet-stream", file);

                        Log("Zip uploaded successfully to the Google..");

                        var Streamlist = UnZipToMemory(file);

                        Log("Unzip process successfully..");

                        foreach (var item in Streamlist)
                        {
                            PushFileToGoogle(credential, Helpers.GOOGLE_CLOUD_GAMES_BUCKET, _file.GameMd5 + "/" + item.Key, GetMimeType(item.Key), item.Value);
                            Log(item.Key + " has been uploaded to the Google..");
                        }
                        
                    }
                    else if (_file.GameType == 1)
                    {
                        PushFileToGoogle(credential, Helpers.GOOGLE_CLOUD_GAMES_FLASH_BUCKET, fileName, "application/x-shockwave-flash", file);
                        Log("Swf has been uploaded to the Google..");
                    }
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return true;
        }

        public static string GetMimeType(string fileName)
        {
            string mimeType = "application/octet-stream";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            if (ext == ".js")
                mimeType = "application/javascript";
            if (ext == ".json")
                mimeType = "application/json";
            return mimeType;
        }
        public void PushFileToGoogle(GoogleCredential credential, string bucket, string fileName, string ContentType, Stream file)
        {
            try
            {
                var storage_ = StorageClient.Create(credential);

                var response = storage_.UploadObject(bucket, fileName, ContentType, file, new UploadObjectOptions { PredefinedAcl = PredefinedObjectAcl.PublicRead });
                var object_ = storage_.GetObject(bucket, _file.GameMd5);
                    
            }
            catch (Exception ex) { Log(fileName + " failed. Exception is " + ex.Message); }

        }
    }
}
