﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;

namespace IPCNet
{
    public class IPCService
    {
        WebServiceHost _serviceHost;
        protected Dictionary<string, InvokeInfo> _invokes=new Dictionary<string,InvokeInfo>();
        protected int _httpPort;
        IPCServiceOptions _options = new IPCServiceOptions();
        public IPCService(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments(args, _options);

            if (_options.HelpRequested)
            {
                Environment.Exit(0);
            }

            RegisterInvoke(new InvokeInfo { Method = "service.api", Handler = new InvokeHandler(Info),Description="Lists all available methods." });

            OpenHTTPService();

            if (_options.AvioosHTTPManagerPath!=null)
                RegisterToAvioosManager();
        }
        private void RegisterToAvioosManager()
        {
            JObject jrequest = new JObject();
            jrequest["method"] = "process.register";

            JObject args = new JObject();
            args["id"] = _options.AvioosId;
            args["pid"] = Process.GetCurrentProcess().Id;
            args["sid"] = Process.GetCurrentProcess().SessionId;
            args["host.http.port"] = _httpPort;
            args["host.address"] = _options.HostAddress;

            jrequest["args"] = args;

            JObject jresponse = AvioosHTTPInvoke(jrequest);
        }
        private void OpenHTTPService()
        {
            _httpPort = _options.HostHTTPPort;
            if (_httpPort == 0)
                _httpPort = HTTPEndpoint.FreeTcpPort();

            HTTPEndpoint httpEndpoint = new HTTPEndpoint(this);

            string webHost = "http://" + _options.HostAddress + ":" + _httpPort;

            Console.WriteLine(webHost);

            _serviceHost = new WebServiceHost(httpEndpoint, new Uri(webHost));

            WebHttpBehavior behavior = new WebHttpBehavior
            {
                AutomaticFormatSelectionEnabled = true,
                HelpEnabled = false
            };

            _serviceHost.AddServiceEndpoint(typeof(IHTTPEndpoint), new WebHttpBinding
            {
                ContentTypeMapper = new HTTPEndpointContentTypeMapper(),
                TransferMode = TransferMode.Streamed,
                MaxReceivedMessageSize = int.MaxValue
            }, "").Behaviors.Add(behavior);

            _serviceHost.Open();
        }
        internal System.IO.Stream Invoke(HTTPEndpoint sender, System.IO.Stream content)
        {
            try
            {
                JObject jrequest = JObject.Load(new JsonTextReader(new StreamReader(content)));
                var method = (string)jrequest["method"];
                var jargs = jrequest["args"];

                JObject jresponse = new JObject();
                jresponse["method"] = method;
                jresponse["result"] = _invokes[method].Handler(jargs);

                sender.OutResponse.ContentType = "application/json; charset=utf-8";

                return jresponse.ToStream();
            }
            catch (Exception exc)
            {
                JObject jresponse = new JObject();
                JObject jerror = new JObject();
                jerror["message"] = exc.Message;
                jresponse["error"] = jerror;
                sender.OutResponse.StatusCode = HttpStatusCode.InternalServerError;
                return jresponse.ToStream();
            }
        }
        public void RegisterInvoke(InvokeInfo handler)
        {
            _invokes[handler.Method] = handler;
        }
        public void RegisterServiceStart(InvokeHandler handler)
        {
            var info = new InvokeInfo() { Method = "service.start", Handler = handler,Description="Starts service. It can be invoked anytime while service is in stopped state." };
            _invokes[info.Method] = info;
        }
        public void RegisterServiceStop(InvokeHandler handler)
        {
            var info = new InvokeInfo() { Method = "service.stop", Handler = handler, Description = "Stops service. It can be invoked anytime while service is in started state." };
            _invokes[info.Method] = info;
        }
        public JToken Info(JToken args)
        {
            JArray result = new JArray();
            foreach (var item in _invokes)
            {
                result.Add(new JObject(new JProperty("method",item.Key),new JProperty("description",item.Value.Description)));
            }

            return result;
        }
        public JObject AvioosHTTPInvoke(JObject jrequest)
        {
            try
            {
                string url = String.Format("http://{0}:{1}/!/{2}/api/invoke", _options.AvioosAddress,_options.AvioosHTTPPort,_options.AvioosHTTPManagerPath);

                var request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                
                if(jrequest["timeout"]!=null)
                {
                    request.Timeout =(int)jrequest["timeout"];
                    request.ReadWriteTimeout= (int)jrequest["timeout"];
                }

                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                jrequest.ToStream(requestStream);
                requestStream.Close();

                var response = request.GetResponse();
                var responseStream = response.GetResponseStream();

                JObject jresponse = JObject.Load(new JsonTextReader(new StreamReader(responseStream)));

                response.Close();

                return jresponse;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Trace.WriteLine(exc.Message);
                return null;
            }
        }
    }
}