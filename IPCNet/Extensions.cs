﻿using System;
using System.IO;
using Newtonsoft.Json.Bson;

namespace Newtonsoft.Json.Linq
{
    public static class JObjectExtension
    {
        public static Stream ToStream(this JObject jobj)
        {
            MemoryStream stream = new MemoryStream();
            JsonWriter jsonWriter = new JsonTextWriter(new StreamWriter(stream));
            jobj.WriteTo(jsonWriter);
            jsonWriter.Flush();
            stream.Position = 0;

            return stream;
        }
        public static void ToStream(this JObject jobj, Stream stream)
        {
            JsonWriter jsonWriter = new JsonTextWriter(new StreamWriter(stream));
            jobj.WriteTo(jsonWriter);
            jsonWriter.Flush();
        }
        public static Stream ToBinaryStream(this JObject jobj)
        {
            MemoryStream stream = new MemoryStream();
            BsonWriter jsonWriter = new BsonWriter(stream);
            jobj.WriteTo(jsonWriter);
            jsonWriter.Flush();
            stream.Position = 0;
            return stream;
        }
        public static JObject ToJObject(this Stream content)
        {
            return JObject.Load(new JsonTextReader(new StreamReader(content)));
        }
        public static JArray BinaryToJArray(this Stream content)
        {
            return JArray.Load(new BsonReader(content, true, DateTimeKind.Utc));
        }
        public static JObject BinaryToJObject(this Stream content)
        {
            return JObject.Load(new BsonReader(content));
        }
        public static JObject BinaryToJObject(this byte[] content)
        {
            using (var reader = new BsonReader(new MemoryStream(content)))
            {
                return JObject.Load(reader);
            }
        }
        public static JObject FromFile(string filename)
        {
            if (!File.Exists(filename)) return null;

            StreamReader reader = new StreamReader(filename, System.Text.UTF8Encoding.UTF8);
            JObject jobj = JObject.Load(new JsonTextReader(reader));
            reader.Close();
            return jobj;
        }
        public static JObject FromBsonFile(string filename)
        {
            if (!File.Exists(filename)) return null;

            using (FileStream file = File.Open(filename, FileMode.Open, FileAccess.Read))
            {
                JObject jobj = JObject.Load(new BsonReader(file));

                return jobj;
            }
        }
        internal static void ToBsonFile(JObject Config, string filename)
        {
            using (FileStream file = File.Create(filename))
            {
                BsonWriter writer = new BsonWriter(file);
                Config.WriteTo(writer);
                writer.Close();
            }
        }
    }

    public static class JArrayExt
    {
        public static JArray FromFile(string filename)
        {
            if (!File.Exists(filename)) return null;

            StreamReader reader = new StreamReader(filename);
            JArray jobj = JArray.Load(new JsonTextReader(reader));
            reader.Close();
            return jobj;
        }
        //public static JArray ToJArray(this string content)
        //{
        //    StreamReader reader = new StreamReader(content.ToStream());
        //    JArray jarr = JArray.Load(new JsonTextReader(reader));
        //    reader.Close();
        //    return jarr;
        //}
        public static JArray ToJArray(this Stream content)
        {
            StreamReader reader = new StreamReader(content);
            JArray jarr = JArray.Load(new JsonTextReader(reader));
            reader.Close();
            return jarr;
        }
    }

    public static class JTokenExtension
    {
        public static object TryConvert(this JToken token, Type conversionType)
        {
            string value = token.ToString();

            if (conversionType == typeof(Guid))
            {
                return new Guid(value);
            }
            return Convert.ChangeType(value, conversionType);
        }
    }
}
