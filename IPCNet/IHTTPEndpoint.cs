﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IPCNet
{
    [ServiceContract]
    interface IHTTPEndpoint
    {
        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        [OperationContract]
        [WebGet(UriTemplate = "api/ping")]
        void Ping();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "api/invoke")]
        Stream Invoke(Stream content);
    }
}