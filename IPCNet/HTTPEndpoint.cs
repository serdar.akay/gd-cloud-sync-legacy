﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;

namespace IPCNet
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single,ConcurrencyMode=ConcurrencyMode.Multiple)]
    public class HTTPEndpoint:IHTTPEndpoint
    {
        IPCService _service;
        public HTTPEndpoint(IPCService service)
        {
            _service = service;
        }
        public void Ping()
        {
            WebOperationContext.Current.OutgoingResponse.Headers["Cache-control"] = "no-cache";
            WebOperationContext.Current.OutgoingResponse.Headers["Pragma"] = "no-cache";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            //WebOperationContext.Current.OutgoingResponse.Headers["Avioos-ServiceTime"] = DateTime.Now.ToString("o");
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
        }

        public System.IO.Stream Invoke(System.IO.Stream content)
        {
            WebOperationContext.Current.OutgoingResponse.Headers["Cache-control"] = "no-cache";
            WebOperationContext.Current.OutgoingResponse.Headers["Pragma"] = "no-cache";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            //WebOperationContext.Current.OutgoingResponse.Headers["Avioos-ServiceTime"] = DateTime.Now.ToString("o");
            return _service.Invoke(this, content);
        }

        public static int FreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }
        public OutgoingWebResponseContext OutResponse
        {
            get
            {
                return WebOperationContext.Current.OutgoingResponse;
            }
        }
        public void GetOptions()
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Origin", "*");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type");
        }
    }

    public class HTTPEndpointContentTypeMapper : WebContentTypeMapper
    {
        public override WebContentFormat
                   GetMessageFormatForContentType(string contentType)
        {
            return WebContentFormat.Raw;
        }
    }
}