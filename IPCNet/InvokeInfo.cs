﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPCNet
{
    public delegate JToken InvokeHandler(JToken args);
    public class InvokeInfo
    {       
        public InvokeHandler Handler { get; set; }
        public string Description { get; set; }
        public string Method { get; set; }
    }
}