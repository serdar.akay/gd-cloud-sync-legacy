﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommandLine.Text;

namespace IPCNet
{
    public class IPCServiceOptions
    {
        [Option("host.address", Required = false, DefaultValue = "127.0.0.1")]
        public string HostAddress { get; set; }

        [Option("host.http.port", Required = false, DefaultValue = 0)]
        public int HostHTTPPort { get; set; }

        [Option("avioos.http.manager", Required = false)]
        public string AvioosHTTPManagerPath { get; set; }

        [Option("avioos.id", Required = false)]
        public string AvioosId{ get; set; }

#if DEBUG
        [Option("avioos.address", Required = false, DefaultValue = "127.0.0.1")]
        public string AvioosAddress { get; set; }

        [Option("avioos.http.port", Required = false, DefaultValue = 22080)]
        public int AvioosHTTPPort { get; set; }
#else
        [Option("avioos.address", Required = false, DefaultValue = "127.0.0.1")]
        public string AvioosAddress { get; set; }

        [Option("avioos.http.port", Required = false, DefaultValue = 12080)]
        public int AvioosHTTPPort { get; set; }
#endif
        [HelpOption]
        public string GetUsage()
        {
            HelpRequested = true;

            return HelpText.AutoBuild(this,
                  (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        public bool HelpRequested { get; set; }
    }
}